# PyTerminal

PyTerminal is a Python module that allows you to print a text with a customizable speed

### Example

```py
Python 2.7.9 (default, Apr  2 2015, 15:33:21) 
[GCC 4.9.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import terminal
>>> terminal.keyboard("Message", 0.05) #where 0.05 in seconds
```
### Version

**0.1** (First version :D)

### Installation

You need Python and Pip installed globally:

```sh
dreamwhite@dreamwhite~$ sudo pip install terminal
```
### or

```sh
dreamwhite@dreamwhite~$ git clone https://github.com/dr34mhw1t3/pyterminal/pyterminal.git
```


License
---- 

 PyTerminal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PyTerminal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Gnu Public License

**Free Software, Hell Yeah!**
